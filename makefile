ASM=nasm
ASMFLAGS=-f elf64
LD=ld

SHELL:=bash


%.o: %.asm 
	$(ASM) $(ASMFLAGS) -o $@ $< 

main.o: main.asm lib.o dist.o
	$(ASM) $(ASMFLAGS) -o $@ $< 

programm: main.o lib.o dist.o
	$(LD) -o $@ $^

.PHONY: clean, test
clean:
	rm *.o 

test:
	@t1="third word" ; \
	v1="third" ; \
	t2="second word" ; \
	v2="second" ; \
	t3="asdfcxzv" ; \
	v3="Строка не найдена" ; \
	t4="asdfgghkjklzxcvkjlkasdfjklhwqebnmzxcvlhkwjebmaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" ; \
	v4="Строка слишком длинная" ; \
	for i in 1 2 3 4 ; \
	do \
	   t="t"$$i ; \
	   v="v"$$i ; \
	   if [ "$$(echo $${!t} | ./programm 2>&1)" = "$${!v}" ] ; \
	        then \
	                echo "Тест $$i пройден "; \
	        else \
	                echo "Тест $$i не пройден" ; \
        	fi \
	done ; \


