%macro colon 2; (ключ, метка значения)
    %2:
        %ifdef begin
            dq begin
        %else
            dq 0
        %endif
        db %1, 0
        %define begin %2
%endmacro
