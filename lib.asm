global exit
global string_length
global string_copy
global string_equals
global print_char
global print_newline
global print_uint
global print_int
global read_char
global read_word
global parse_uint
global parse_int
global print_string
global print_err_string

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    xor rdi, rdi
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop_string_length:
        cmp byte [rdi+rax], 0
        je .end_string_length
        inc rax
        jmp .loop_string_length
    .end_string_length:
        ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret


print_err_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 2
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdx, 1
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xa
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    dec rsp
    mov byte[rsp], 0x0
    mov rcx, 10
    mov rsi, 1
    .loop_print_uint:
        xor rdx, rdx
        div rcx
        add rdx, '0'
        dec rsp,
        mov [rsp], dl
        inc rsi
        test rax, rax
        jz .end_print_uint
        jmp .loop_print_uint
    .end_print_uint:
        mov rdi, rsp
        push rsi
        call print_string
        pop rsi
        add rsp, rsi
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    test rdi, rdi
    jns print_uint
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    push rdx
    xor rdx, rdx
    .loop_string_equals: 
        mov al, [rsi+rdx]
        mov cl, [rdi+rdx]
        inc rdx
        cmp al, cl
        jne .not_equal
        cmp al, 0
        je .equal
        jmp .loop_string_equals
    .equal:
        mov rax, 1
        jmp .end_string_equals
    .not_equal:
        xor rax, rax
        jmp .end_string_equals
    .end_string_equals:
        pop rdx
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    dec rsp
    mov rsi, rsp
    mov rdx, 1
    syscall
    test rax, rax
    jz .eof
    mov al, [rsp]
    jmp .end_read_char
    .eof:
        xor rax, rax
    .end_read_char:
        inc rsp
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rdx, rdx
    xor rax, rax
    .loop_read_word:
        push rsi
        push rdi
        push rdx
        call read_char
        pop rdx
        pop rdi
        pop rsi

        cmp rax, 0
        je .end_read_word
        ;cmp rax, 0x20
        ;je .space
        cmp rax, 0xa
        je .end_read_word
        ;cmp rax, 0x9
        ;je .space

        mov  byte[rdi+rdx], al
        inc rdx
        cmp rdx, rsi 
        jge .overflow_read_word
        jmp .loop_read_word
    ;.space:
    ;    cmp rdx, 0
    ;    jne .end_read_word
    ;    jmp .loop_read_word
    .overflow_read_word:
        xor rax, rax
        xor rdx, rdx
        ret
    .end_read_word:
        mov byte[rdi+rdx], 0
        mov rax, rdi
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    xor r8, r8
    mov r9, 10
    .loop_parse_uint:
        mov r8b, byte[rdi+rcx]
        cmp r8b, '0'
        jb .end_parse_uint
        cmp r8b, '9'
        ja .end_parse_uint
        sub r8b, '0'
        inc rcx
        mul r9
        add rax, r8
        jmp .loop_parse_uint
    .end_parse_uint:
        mov rdx, rcx
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je .negative
    cmp byte[rdi], '+'
    je .positive
    jmp parse_uint
    .positive:
        inc rdi
        call parse_uint
        inc rdx
        ret
    .negative:
        inc rdi
        call parse_uint
        inc rdx
        neg rax
        ret
 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length
    cmp rax, rdx
    jg .overflow_string_copy
    xor rax, rax
    .loop_string_copy:
        mov r8b, [rdi+rax]
        mov [rsi+rax], r8b
        cmp r8b, 0
        je .end_string_copy
        inc rax
        jmp .loop_string_copy
    .overflow_string_copy:
        xor rax, rax
        ret
    .end_string_copy:
        ret

