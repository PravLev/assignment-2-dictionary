%include "lib.inc"

global find_world

section .text

find_world: ;(rdi = указатель на строку, rsi = указатель на начало массива)
    .loop_find_word:
        test rsi, rsi
        jz .not_found
        push rdi
        push rsi
        add rsi, 8
        call string_equals
        pop rsi
        pop rdi
        test rax, rax
        jnz .found
        mov rsi, [rsi]
        jmp .loop_find_word
    .found:
        mov rax, rsi
        ret
    .not_found:
        xor rax, rax
        ret
