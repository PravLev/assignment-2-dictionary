%include "lib.inc"
%include "colon.inc"
%include "words.inc"
extern find_world

%define size_buf 256

section .bss
    buf: resb size_buf    

section .rodata
    string_to_long: db "Строка слишком длинная", 0
    string_not_found: db "Строка не найдена", 0

section .text

global _start

    _start:
        mov rdi, buf
        mov rsi, size_buf
        call read_word
        test rax, rax
        jne .not_long
        mov rdi, string_to_long
        jmp .error
    .not_long:
        mov rdi, buf
        mov rsi, begin
        call find_world
        test rax, rax 
        jne .found
        mov rdi, string_not_found
        jmp .error
    .found:
        mov rdi, rax
        add rdi, 8
        push rdi
        call string_length
        pop rdi 
        add rdi, rax
        inc rdi 
        call print_string
        call print_newline
        xor rdi, rdi
        call exit
    .error: 
        call print_err_string
        call print_newline
        mov rdi, 1
        call exit
